# data4tests

## What it contains ##

Rules and conventions for data sets as Input/Output files in `diodon` (`py` and `cpp`)


The same conventions are used for `input` files and for `output` files.


**note:** There is a slight ambiguity about the notion of dataset which cannot be removed here. A data set (two words) is a set of data ... and a dataset (one word) is an object in a hdf5 file. Here, we adopt the convention:

<ul>
<li>a data set (in two words) is an input/output file for a method</li>
<li>a dataset is an object in a hdf5 file</li>
</ul>


##Types of datasest

Globally, a data set is a 2D array with rows and columns, with metadata.     


Most standard metadata are

<ul>
<li>row names</li>
<li> column names, often called headers</li>
</ul>


The following situations hare handled by `diodon`  

| Values of the array |rownames | headers |
|----------|----------------|-----------|
| compulsory | yes | yes |
| compulsory | yes | no |
| compulsory | no | yes |
| compulsory | no | no |


Let us note that

<ul>
<li>some methods have one dataset as Input: PCA, CoA, etc. ...</li>
<li> some others have two or several datasets as input: PCA-vi, Can, MCA, etc ... </li>
</ul>

This leads to finding a good balance between flexibiliy for handling different situations and genericity for offering to users un common framework. This is the objective of this `readme`.

## Formats##

Different formats are recognized by their suffix, as follows:


| format       | extension     | 
| ------------- |------------------|
| ascii        | .txt, .csv |
| compressed | .zip, .gz, .bz2 |
| hdf5      | .h5 |


##Delimiters in `ascii` format##

Delimiters are defined for ascii format. We encourage the following convention for correspondence between delimiters and suffixes: 


|delimiter | suffix |
|------------|---------|
| \t | .txt |
| blank | .csv |
|comma | .csv |
|semi-column | .csv|

##Metadata##

Metadata other than rownames or headers are handled  

<ul>
<li>in a `yaml` file for datasets in ascii format</li>
<li> as datasets or attributes for datasets in `hdf5` format</li>
</ul>


##Templates##

Some templates are given in this directory, as follows.

###an example: template for PCA###

The file name is `pca_template`. It has two versions:

|name| metadata |
| -------|--------------|
|pca_template_withnames| with rownames and headers |
|pca_template_noname | without rownames and headers |

which are given each in different formats. The following files (content, format) can be downloaded on the example of pca_template_withnames. The equivalent is given for files ca_templ	ate_noname.[suffix].  

|file name | row and column names |  ascii | delimiter |compressed | hdf5 |
|-------------|--------------------------------|---------|-------|----------|-------|
|pca_template_withnames.txt  | yes | yes |   \t  | no   | no |
|pca_template_withnames.csv | yes | yes |  , or ; or blank   | no    | no |
|pca_template_withnames.txt.zip  | yes | yes |   \t  | zip   | no |
|pca_template_withnames.csv.zip | yes | yes |  , or ; or blank   | zip    | no |
|pca_template_withnames.txt.gz  | yes | yes |   \t  | gz   | no |
|pca_template_withnames.csv.gz | yes | yes |  , or ; or blank   | gz    | no |
|pca_template_withnames.txt.bz2  | yes | yes |   \t  | bz2   | no |
|pca_template_withnames.csv.bz2 | yes | yes |  , or ; or blank   | bz2    | no |
|pca_template_withnames.h5  | yes | yes |   stream  | yes   | yes |



af & jmf, started February 25, 2021
version 21.02.25

##Which data sets?##

Each method has one or several data sets as inputs, and one or several data sets as outputs. THis is recalled here. 


|method| # dataset  | input | rows | columns |
|----------|-------| --------| -----------|-----|
| PCA | 1 | A | individuals | variables | 
|PCA-IV |   1 | A | individuals | variables I | 
| |  2 | B | individuals (same than 1) | variables II| 
|CAN |   1 | A | individuals | variables I | 
| |  2 | B | individuals (same than 1) | variables II| 
| CoA | 1 | A | variable I | variable II |

##How files are read or written##

For the lazy user, we have implemeted in `python` or `Cpp` a function `diodon.load()`which can be called with no other argument than the file name. There is behind a specific function fo each format. The format can be recognized from the suffix, or (safer), given as another argument.



The same is proposed for writing result files. 




